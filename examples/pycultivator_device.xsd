<?xml version="1.0" encoding="UTF-8"?>
<xs:schema version="2.0" xmlns:xs="http://www.w3.org/2001/XMLSchema" targetNamespace="pyCultivator"
           xmlns:pc="pyCultivator"
           attributeFormDefault="unqualified" elementFormDefault="qualified">
	<xs:annotation>
		<xs:documentation>
			This schema contains the configuration for one general type of pyCultivator device.
			Other packages provide more specific implementations or more flexible implementations.
			The schema assumes that all necessary classes - corresponding to the elements in this schema -
			are known by the user.
		</xs:documentation>
	</xs:annotation>
	<xs:include schemaLocation="pycultivator.xsd"/>
	<xs:element name="device" type="pc:deviceType"/>

	<!-- Complex ComplexDevice-->
	<xs:complexType name="deviceType">
		<xs:annotation>
			<xs:documentation>
				The device elements holds all information required for using this device
			</xs:documentation>
		</xs:annotation>
		<xs:all>
			<!-- overall device settings -->
			<xs:element name="settings" type="pc:settingsType"/>
			<!-- connection settings, such as port, speed etc -->
			<xs:element name="connection" type="pc:connectionType"/>
			<!-- default settings for instrument settings -->
			<!-- in the case of multiple types of instruments -->
			<!-- defaults are matched by type -->
			<xs:element name="instruments" type="pc:instrumentsType"/>
			<!-- specific definition of channels in the device -->
			<xs:element name="channels" type="pc:channelsType"/>
		</xs:all>
		<xs:attribute name="id" type="xs:string" use="required" />
		<xs:attribute name="class" type="xs:string" use="optional"/>
		<xs:attribute name="schema_version" type="xs:string" use="required" />
	</xs:complexType>

	<!-- Complex Connection -->
	<xs:complexType name="connectionType">
		<xs:annotation>
			<xs:documentation>An connection describes how to connect to the device</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="settings" type="pc:settingsType"/>
		</xs:sequence>
		<xs:attribute name="class" type="xs:string" use="optional"/>
	</xs:complexType>

	<!-- Complex Instruments -->
	<xs:complexType name="instrumentsType">
		<xs:annotation>
			<xs:documentation>Instruments contains the instruments in the device</xs:documentation>
		</xs:annotation>
		<xs:sequence minOccurs="0" maxOccurs="unbounded">
			<xs:element ref="pc:instrument"/>
		</xs:sequence>
	</xs:complexType>
	<xs:element name="instrument" type="pc:instrumentType" />
	<xs:complexType name="instrumentType">
		<xs:annotation>
			<xs:documentation>
				An Instrument definition holds all information required to use the instrument
			</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="settings" type="pc:settingsType" maxOccurs="1" minOccurs="0"/>
			<xs:element name="calibration" type="pc:calibration" maxOccurs="1" minOccurs="0"/>
		</xs:sequence>
		<xs:attribute name="id" type="xs:string" use="required"/>
		<xs:attribute name="class" type="xs:string" use="optional"/>
	</xs:complexType>

	<!-- Complex Channels -->
	<xs:complexType name="channelsType">
		<xs:annotation>
			<xs:documentation>Contains all (cultivation)channels in the device</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="channel" type="pc:channelType" minOccurs="1" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="channelType">
		<xs:annotation>
			<xs:documentation>Holds all information specific to this channel</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="settings" type="pc:settingsType" minOccurs="0" maxOccurs="1"/>
			<xs:element name="instruments" type="pc:instrumentsType" minOccurs="0" maxOccurs="1"/>
		</xs:sequence>
		<xs:attribute name="id" type="xs:string" use="required"/>
	</xs:complexType>

	<!-- Complex Calibration -->
	<xs:complexType name="calibration">
		<xs:annotation>
			<xs:documentation>A calibration defines a relation between a input signal and output</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="pc:definitionType">
				<xs:sequence>
					<xs:element name="polynomial" type="pc:polynomialRelationType" minOccurs="0" maxOccurs="unbounded"/>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
</xs:schema>
