"""
The pycultivator-lcp package provides bindings to the LightCalibrationPlate



"""

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


def register():
    """Registers this package with pycultivator

    - registers the lcp:// and fakelcp:// url's
    - registers the config classes
    - registers the data model

    """
    pass

