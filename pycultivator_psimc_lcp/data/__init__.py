# coding=utf-8
"""
The pycultivator_psimc_lcp.data package provides the DataModel for the MCLCPlate.
"""

from pycultivator.foundation import uvaLog
# from pycultivator.data import registry

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'

# connect to logger
uvaLog.uvaLogger.createLogger(__name__).debug("Connected {} to logger".format(__name__))


# def register():
#     """Register"""
#     # add the labjack connection
#     if not registry.isRegistered("labjack"):
#         registry.register(
#             "pycultivator_mc_lcp.connection.LabJackConnect.LabJackConnection", "labjack"
#         )
#     else:
#         uvaLog.uvaLogger.createLogger(__name__).debug("A labjack connect has already been registered")
