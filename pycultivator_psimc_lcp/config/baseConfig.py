"""
This module provides retrieving and storing functionality for the light calibration plate
"""
from pycultivator.config import baseConfig
from pycultivator.config.baseConfig import (
    CalibrationConfig,
    PolynomialConfig,
    SettingsConfig
)
from pycultivator_psimc_lcp.instrument import Diode
from pycultivator_psimc_lcp.device import MCLCPlate
from pycultivator.connection import LabJackConnection

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class DeviceConfig(baseConfig.DeviceConfig):
    """Config class to create/save/update the device objects from/to the configuration source"""

    _configures = MCLCPlate.MCLCPlate
    _configures_type = set().union(["plate"], baseConfig.DeviceConfig.configuresTypes())

    def __init__(self, source, settings=None, **kwargs):
        """Initialise the configuration object of a device

        :type source: pycultivator_psimc_lcp.config.Config.Config.Config
        """
        if not isinstance(source, Config):
            raise ConfigException("Expected a Light Calibration Plate Configuration Source")
        super(DeviceConfig, self).__init__(source=source, settings=settings, **kwargs)

    def find(self, name=None, root=None):
        raise NotImplementedError

    def read(self, definition, parent=None, template=None):
        raise NotImplementedError

    def readDefaultChannel(self, definition, d, template=None):
        return super(DeviceConfig, self).readDefaultChannel(definition, d, template=template)

    def save(self, device):
        """Store the object in the configuration source"""
        raise NotImplementedError


class ConnectionConfig(baseConfig.ConnectionConfig):
    """Config class for handling the configuration of a connection object from a configuration source"""

    _configures = LabJackConnection.LabJackConnection

    def __init__(self, source, settings=None, **kwargs):
        """Initialise the configuration object of a device

        :type source: pycultivator_psimc_lcp.config.Config.Config.Config
        """
        if not isinstance(source, Config):
            raise ConfigException("Expected a Light Calibration Plate Configuration Source")
        super(ConnectionConfig, self).__init__(source=source, settings=settings, **kwargs)

    def find(self, root=None):
        raise NotImplementedError

    def read(self, definition, template=None):
        raise NotImplementedError

    @classmethod
    def loadDefault(cls):
        return LabJackConnection.LabJackConnection()

    def loadFakeDefault(cls):
        return LabJackConnection.FakeLabJackConnection()

    def save(self, device):
        """Store the object in the configuration source"""
        raise NotImplementedError


class ChannelConfig(baseConfig.ChannelConfig):
    """Config class for handling the configuration of a connection object from a configuration source"""

    _configures = MCLCPlate.MCLCPlateChannel

    def __init__(self, source, settings=None, **kwargs):
        """Initialise the configuration object of a device

        :type source: pycultivator_psimc_lcp.config.Config.Config.Config
        """
        if not isinstance(source, Config):
            raise ConfigException("Expected a Light Calibration Plate Configuration Source")
        super(ChannelConfig, self).__init__(source=source, settings=settings, **kwargs)

    def find(self, name=None, root=None):
        raise NotImplementedError

    # Behaviour methods

    def read(self, definition, parent=None, template=None):
        raise NotImplementedError

    def save(self, channel):
        """Store the object in the configuration source"""
        raise NotImplementedError


class DiodeConfig(baseConfig.InstrumentConfig):
    """Config class to handle the diode configuration from the configuration source"""

    _configures = Diode.Diode
    _configures_type = ["diode"]

    def __init__(self, source, settings=None, **kwargs):
        """Initialise the configuration object of a device

        :type source: pycultivator_psimc_lcp.config.Config.Config.Config
        """
        if not isinstance(source, Config):
            raise ConfigException("Expected a Light Calibration Plate Configuration Source")
        super(DiodeConfig, self).__init__(source=source, settings=settings, **kwargs)

    def find(self, name=None, root=None):
        raise NotImplementedError

    # Behaviour methods

    def read(self, definition, parent=None, template=None):
        raise NotImplementedError

    def save(self, diode):
        """Store the object in the configuration source"""
        raise NotImplementedError


class Config(baseConfig.Config):
    """ The config class provides the API to configure a light calibration plate object."""

    _namespace = "config"
    # set available helpers
    _known_helpers = [
        # baseConfig.ObjectConfig,
        # baseConfig.PartConfig,
        DeviceConfig,
        ConnectionConfig,
        ChannelConfig,
        DiodeConfig,
        CalibrationConfig,
        PolynomialConfig,
        SettingsConfig
    ]

    default_settings = baseConfig.Config.mergeDefaultSettings(
        {
            # set extra settings
        }, namespace=_namespace
    )

    def __init__(self, settings=None, **kwargs):
        super(Config, self).__init__(settings, **kwargs)

    @classmethod
    def load(cls, location, settings=None, **kwargs):
        """ABSTRACT Loads the source into memory and creates the config object

        :param location: Location from which the configuration will be loaded
        :type location: str
        :param settings: Settings dictionary that should be loaded into the configuration
        :type settings: None or dict[str, object]
        :rtype: None or pycultivator_plate.config.Config.Config
        """
        raise NotImplementedError

    @classmethod
    def check(cls, location):
        """ABSTRACT Checks whether the source can be loaded into the configuration object

        :param location: Location that has to be checked
        :type location: str
        :rtype: bool
        """
        raise NotImplementedError


class ConfigException(baseConfig.ConfigException):
    """An Exception raised by the baseConfig classes"""

    def __init__(self, msg):
        super(ConfigException, self).__init__(msg)
