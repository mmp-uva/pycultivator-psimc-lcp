"""
This module provides configuration of the light calibration plate from XML Files
"""

from pycultivator.config import XMLConfig as baseXMLConfig
from pycultivator.config.XMLConfig import XMLCalibrationConfig, XMLPolynomialConfig, XMLSettingsConfig
# import base lcp configuration
import Config as baseConfig

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class XMLDeviceConfig(baseXMLConfig.XMLDeviceConfig, baseConfig.DeviceConfig):
    """Config class for handling the Light Calibration Plate Object from XML"""

    def __init__(self, source, settings=None, **kwargs):
        if not isinstance(source, XMLConfig):
            raise baseXMLConfig.XMLConfigException("Expected a XMLConfig as source")
        baseXMLConfig.XMLDeviceConfig.__init__(self, source=source, settings=settings, **kwargs)

    def loadDefaultChannel(self, element, parent=None, template=None):
        """Loads the configuration of the default channel

        :type element: lxml.etree._Element._Element
        :type template: None or pycultivator_lcp.device.MCLCPlate.MCLCPlateChannel
        :rtype: pycultivator_lcp.device.MCLCPlate.MCLCPlateChannel
        """
        # loads the channel config helper
        cHelper = self.getSource().getHelperFor(baseConfig.MCLCPlate.MCLCPlateChannel)
        """:type: pycultivator_psimc_lcp.config.XMLConfig.XMLChannelConfig"""
        # load default template
        if template is None:
            template = cHelper.loadDefault(parent)
        # check template type
        self.assertTemplateType(template, baseConfig.MCLCPlate.MCLCPlateChannel)
        # copy channel to work instance
        channel = template.copy()
        # load the instrument config helper
        dHelper = self.getSource().getHelperFor(baseConfig.Diode.Diode)
        """:type: pycultivator_psimc_lcp.config.XMLConfig.XMLDiodeConfig"""
        # find default instruments element
        i_element = self._find("instruments", root=element)
        i_element = i_element[0] if len(i_element) > 0 else element
        # find all diodes, no need to give parent
        diodes = dHelper.loadAll(root=i_element)
        # add them to the default channel
        for diode in diodes:
            channel.addInstrument(diode)
        return channel

    @classmethod
    def loadDefault(cls, parent=None):
        return baseConfig.DeviceConfig.loadDefault(parent)


class XMLConnectionConfig(baseXMLConfig.XMLConnectionConfig, baseConfig.ConnectionConfig):
    """Config class for handling the configuration of a LabJack connection object from a configuration source"""

    def __init__(self, source, settings=None, **kwargs):
        if not isinstance(source, XMLConfig):
            raise baseXMLConfig.XMLConfigException("Expected a XMLConfig as source")
        baseXMLConfig.XMLConnectionConfig.__init__(self, source=source, settings=settings, **kwargs)

    def parse(self, element, template=None):
        """Creates a new ComplexDevice object from a XML Element

        :type element: lxml.etree._Element._Element
        :type template: None or pycultivator.connection.Connection.Connection
        :rtype: pycultivator.connection.Connection.Connection
        """
        if template is None:
            template = self.loadDefault()
        # test if element can be parsed, if not raise Exception
        self.assertValidElement(element)
        # test if template is of correct type
        self.assertTemplateType(template, baseConfig.LabJackConnection.LabJackConnection)
        # gather information
        settings = self.getSource().getHelperFor(dict).load(root=element)
        """:type: dict[str, object]"""
        # create device
        if self.usesFakeConnection() or settings.get("fake.connection", False) is True:
            connection = baseConfig.LabJackConnection.FakeLabJackConnection()
        else:
            connection = baseConfig.LabJackConnection.LabJackConnection()
        # load default settings into the new connection
        template.copy(connection)
        # apply settings
        connection.mergeSettings(settings)
        return connection

    @classmethod
    def loadDefault(cls):
        return baseConfig.ConnectionConfig.loadDefault()


class XMLChannelConfig(baseXMLConfig.XMLChannelConfig, baseConfig.ChannelConfig):

    def __init__(self, source, settings=None, **kwargs):
        if not isinstance(source, XMLConfig):
            raise baseXMLConfig.XMLConfigException("Expected a XMLConfig as source")
        baseXMLConfig.XMLChannelConfig.__init__(self, source=source, settings=settings, **kwargs)

    def parse(self, element, parent=None, template=None):
        if template is None:
            template = self.loadDefault()
        # test if template is of correct type
        self.assertTemplateType(template, baseConfig.MCLCPlate.MCLCPlateChannel)
        return super(XMLChannelConfig, self).parse(element, parent=parent, template=template)

    @classmethod
    def loadDefault(cls, parent=None):
        return baseConfig.ChannelConfig.loadDefault(parent)


class XMLDiodeConfig(baseXMLConfig.XMLPartConfig, baseConfig.DiodeConfig):
    """Config class to handle the diode configuration from the configuration source"""

    XML_ELEMENT_TAG = "diode"
    XML_ELEMENT_SCOPE = ".//"

    def __init__(self, source, settings=None, **kwargs):
        """Initialise the configuration object"""
        if not isinstance(source, XMLConfig):
            raise baseXMLConfig.XMLConfigException("Expected a XMLConfig as source")
        baseConfig.DiodeConfig.__init__(self, source=source, settings=settings, **kwargs)

    def retrieve(self, diode, root=None):
        """ Searches for the definition of this instrument object in the configuration source

        :param diode:
        :type diode: pycultivator_lcp.instrument.Diode.Diode
        :param root: Root element, only consider children of this element
        :type root: None or lxml.etree._Element._Element
        :return: The found element or None
        :rtype: None or lxml.etree._Element._Element
        """
        return super(XMLDiodeConfig, self).retrieve(diode, root=root)

    # Behaviour methods

    def parse(self, element, parent=None, template=None):
        """Parse a element into a diode object

        :param element: The root element containing all the diode
        :type element: lxml.etree._Element._Element
        :param parent: The parent of this instrument (a channel)
        :type parent: pycultivator_lcp.device.MCLCPlate.MCLCPlateChannel
        :return: The created Diode object
        :rtype: pycultivator_lcp.instrument.Diode.Diode
        """
        if template is None:
            template = self.loadDefault()
        # test if element is correct and can be loaded
        self.assertValidElement(element)
        # gather information
        name = self.parseValue(self.getXML().getId(element), str, default_value="0")
        settings = self.getSource().getHelperFor(dict).load(root=element)
        """:type: dict[str, object]"""
        # now retrieve default diode from the channel
        if parent is not None and parent.hasInstrument(name):
            template = parent.getInstrument(name)
        self.assertTemplateType(template, baseConfig.Diode.Diode)
        # create diode
        diode = template.copy()
        """:type: pycultivator_psimc_lcp.instrument.Diode.Diode"""
        # apply information
        diode.setName(name)
        diode.mergeSettings(settings)
        diode.setAddress(self.getXML().getAttribute(element, "address", vtype=int))
        diode.setGainIndex(self.getXML().getAttribute(element, "gain", default=1, vtype=int))
        diode.setResolutionIndex(self.getXML().getAttribute(element, "resolution", default=12, vtype=int))
        # collect calibrations
        calHelper = self.getSource().getHelperFor(baseConfig.baseConfig.Calibration.Calibration)
        calibrations = []
        if calHelper is not None:
            calibrations = calHelper.loadAll(root=element)
        for calibration in calibrations:
            diode.setCalibration(calibration)
        return diode

    @classmethod
    def loadDefault(cls, parent=None):
        return baseConfig.DiodeConfig.loadDefault(parent)

    def save(self, diode):
        pass

    def update(self, diode):
        pass


class XMLConfig(baseXMLConfig.XMLConfig, baseConfig.Config):
    """Config class for handling the Light Calibration Plate XML File"""

    # set available helpers

    _known_helpers = [
        # baseXMLConfig.XMLObjectConfig,
        # baseXMLConfig.XMLPartConfig,
        XMLDeviceConfig,
        XMLConnectionConfig,
        XMLChannelConfig,
        XMLDiodeConfig,
        XMLCalibrationConfig,
        XMLPolynomialConfig,
        XMLSettingsConfig,
    ]
    COMPATIBLE_VERSION = "1.1"

    @classmethod
    def load(cls, path, settings=None, **kwargs):
        """Loads the source into memory and creates the config object

        :param path: Location from which the configuration will be loaded
        :type path: str
        :param settings: Settings dictionary that should be loaded into the configuration
        :type settings: None or dict[str, object]
        :rtype: None or pycultivator_lcp.config.Config.Config
        """
        return super(XMLConfig, cls).load(path, settings=settings, **kwargs)


class XMLConfigException(baseConfig.ConfigException):
    """Exception raised by the MCLCPlate XML Config"""

    def __init__(self, msg):
        super(XMLConfigException, self).__init__(msg)
