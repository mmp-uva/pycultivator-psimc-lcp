"""
Module for testing the LCP XMLConfiguration Object
"""

from pycultivator.config.tests import test_xmlConfig
from pycultivator_lcp.config.tests import test_Config
from pycultivator_lcp.config import XMLConfig
import os

from nose import SkipTest

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class testXMLConfig(test_xmlConfig.TestXMLConfig):
    """Class for testing the lcp configuration object"""

    _abstract = False
    _subject_cls = XMLConfig.XMLConfig

    _examples_path = os.path.join(os.path.abspath(os.path.dirname(__file__)), "..", "..", "..", "examples")
    _config_path = os.path.join(_examples_path, "configuration.xml")
    _schema_path = os.path.join(_examples_path, "pycultivator-psimc-lcp.xsd")

    def initSubject(self, *args, **kwargs):
        return self.getSubjectClass().load(self.getConfigPath())

    def getSubject(self):
        """Return reference to the instance of the class subjected to this test

        :rtype: pycultivator_lcp.config.Config.Config
        """
        return test_xmlConfig.TestXMLConfig.getSubject(self)

    def getConfigPath(self):
        return self._config_path

    def getSchemaPath(self):
        return self._schema_path

    def test_open(self):
        """Test opening a XMLConfiguration file"""
        pass

    def test_check(self):
        """Test checking a XMLConfiguration file"""
        self.assertTrue(self.getSubjectClass().check(self.getConfigPath()))


class testXMLDiodeConfig(test_Config.TestDiodeConfig):
    """Class for testing the lcp XMLDiodeConfig object"""

    _abstract = False
    _subject_cls = XMLConfig.XMLDiodeConfig

