"""
Module for testing the LCP Configuration Object
"""

from pycultivator.config.tests import test_baseConfig
import os
from nose import SkipTest

from pycultivator_lcp.config import Config

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class TestConfig(test_baseConfig.TestConfig):
    """Class for testing the lcp configuration object"""

    _abstract = True
    _subject_cls = Config.Config

    def getSubject(self):
        """Return reference to the instance of the class subjected to this test

        :rtype: pycultivator_lcp.config.Config.Config
        """
        return test_baseConfig.TestConfig.getSubject(self)

    def test_open(self):
        """Test opening a XMLConfiguration file"""
        pass

    def test_check(self):
        """Test checking a XMLConfiguration file"""
        pass


class TestDiodeConfig(test_baseConfig.TestPartConfig):
    """Class for testing the lcp DiodeConfig object"""

    _abstract = True
    _subject_cls = Config.DiodeConfig
