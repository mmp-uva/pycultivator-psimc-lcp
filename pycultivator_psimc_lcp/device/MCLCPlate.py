"""
The LightCalibrationPlate class provides an API to the Light Calibration Plate
"""

from pycultivator.device import device
from pycultivator_mc_lcp.instrument.Diode import Diode

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class MCLCPlate(device.ComplexDevice):
    """ The MCLCPlate device """

    _namespace = "lcp.device"

    _default_settings = device.ComplexDevice.mergeDefaultSettings(
        {
            "serialNr": None,
            "localID": None,
            "devNr": None
        }, namespace=_namespace
    )

    MAX_CHANNELS = 8

    def __init__(self, name, parent, settings=None, **kwargs):
        super(MCLCPlate, self).__init__(self, name=name, parent=parent, settings=settings, **kwargs)

    def getSerialNumer(self):
        """Returns the serial number of the plate, used to identify the plate over USB"""
        return self.getSetting("serialNr")

    def setSerialNumber(self, serialNr):
        self.setSetting("serialNr", serialNr)
        return self.getSerialNumer()

    def getLocalID(self):
        """Returns the local ID of the plate, used to identify the plate over USB"""
        return self.getSetting("localID")

    def setLocalID(self, localID):
        self.setSetting("localID", localID)
        return self.getLocalID()

    def getDevNumber(self):
        """Returns the development number of the plate, used to identify the plate over USB"""
        return self.getSetting("devNr")

    def setDevNumber(self, devNr):
        self.setSetting("devNr", devNr)
        return self.getDevNumber()

    def getConnection(self):
        """Returns the connection used to connect to the device

        :return: The connection being used.
        :rtype: pycultivator_lcp.connection.LabJackConnection.LabJackConnection
        """
        return super(MCLCPlate, self).getConnection()

    def createConnection(self, fake=False, **kwargs):
        """Creates a new connection and uses it for this device"""
        if not fake:
            from pycultivator.connection.LabJackConnection import LabJackConnection
            self.setConnection(LabJackConnection(**kwargs))
        else:
            from pycultivator.connection.LabJackConnection import FakeLabJackConnection
            self.setConnection(FakeLabJackConnection(**kwargs))
        return self.getConnection()

    # Behaviour methods

    def connect(self, fake=False):
        """Connect to the Light Calibration Plate"""
        result = False
        if not self.hasConnection():
            self.createConnection()
        if self.hasConnection():
            # set parameters
            result = self.getConnection().connect(serialNr=self.getSerialNumer(), localID=self.getLocalID())
        return result


    def measure(self):
        """ Performs measurement routine on all channels and diodes.

        :return: List of measurements
        :rtype: list[]
        """
        results = []
        for channel in self.getChannels().values():
            results.extend(channel.measure())
        return results


class MCLCPlateChannel(device.DeviceChannel):
    """A channel in a MCLCPlate"""

    _namespace = "lcp.channel"

    _default_settings = device.DeviceChannel.mergeDefaultSettings(
        {

        }, namespace=_namespace
    )

    def __init__(self, name, parent, settings=None, **kwargs):
        super(MCLCPlateChannel, self).__init__(self, name=name, parent=parent, settings=settings, **kwargs)

    def measure(self):
        """ Performs measurement routine on all diodes in this channel"""
        results = []
        for instrument in self.getInstrumentsOfType(Diode).values():
            results.append(instrument.measure())
        return results


class MCLCPlateException(device.DeviceException):
    """An exception raised by the MCLCPlate class"""

    pass
